package com.corebanking.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.corebanking.api.entity.Cliente;

public interface ClientesRepository extends JpaRepository<Cliente, Integer> {
	
		@Query(value = "{call UPDINERSION()}", nativeQuery = true)
		String sp_update();
}
