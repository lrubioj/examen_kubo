package com.corebanking.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.corebanking.api.entity.Cliente;
import com.corebanking.api.service.IClientesService;

@RestController
@RequestMapping("/api")
public class ClienteController {

		@Autowired
		private IClientesService serviceClientes;
		
		@GetMapping("/clientes")
		public List<Cliente> buscarTodos(){
			
			return serviceClientes.buscarTodos();
		}
		
		@PostMapping("/clientes")
		public Cliente guardar(@RequestBody Cliente cliente) {
			
			serviceClientes.guardar(cliente);
			return cliente;
		}
		
		@GetMapping("/update")
		public String listaClientes(){
			return serviceClientes.sp_update();
		}
}
