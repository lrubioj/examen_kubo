package com.corebanking.api.service.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.corebanking.api.entity.Cliente;
import com.corebanking.api.repository.ClientesRepository;
import com.corebanking.api.service.IClientesService;

@Service
public class ClientesService implements IClientesService {

	@Autowired
	private ClientesRepository repoClientes;
	@Override
	public List<Cliente> buscarTodos() {
		
		return repoClientes.findAll();
	}
	@Override
	public void guardar(Cliente cliente) {
		// TODO Auto-generated method stub
		
		repoClientes.save(cliente);
	}
	
	public String sp_update() {
		// TODO Auto-generated method stub
		return  repoClientes.sp_update();
	}

}
