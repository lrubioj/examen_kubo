package com.corebanking.api.service;

import java.util.List;

import com.corebanking.api.entity.Cliente;

public interface IClientesService {

		List<Cliente> buscarTodos();
		
		void guardar(Cliente cliente);
		
		String sp_update();
}
