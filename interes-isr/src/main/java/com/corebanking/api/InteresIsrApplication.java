package com.corebanking.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InteresIsrApplication {

	public static void main(String[] args) {
		SpringApplication.run(InteresIsrApplication.class, args);
	}

}
