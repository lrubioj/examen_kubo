
delimiter //

create  procedure kubofinanciero.UPDINERSION()
begin

DECLARE exit handler for SQLEXCEPTION
 BEGIN
  GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
   @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
  SET @full_error = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @text);
  SELECT @full_error as mensaje;
 END;
	
    SET SQL_SAFE_UPDATES = 0;
    
   set @table := 'kubofinanciero.PAGARES_RESPALDO';
    
    set @sql_text := concat('CREATE TEMPORARY TABLE ',@table,' (
  `PagareID` int(12) NOT NULL,
  `CuentaID` int(12) NOT NULL,
  `ClienteID` int(11) DEFAULT NULL,
  `TipoInversionID` int(11) NOT NULL,
  `FechaInicio` date NOT NULL ,
  `FechaVencimiento` date NOT NULL  ,
  `MontoInversion` decimal(12,2) DEFAULT NULL,
  `Plazo` int(11) NOT NULL  ,
  `Tasa` decimal(12,2) DEFAULT NULL,
  `TasaISR` decimal(12,2) DEFAULT NULL,
  `TasaNeta` decimal(12,2) DEFAULT NULL,
  `InteresGenerado` decimal(12,2) DEFAULT NULL,
  `InteresRecibir` decimal(12,2) DEFAULT NULL,
  `InteresRetener` decimal(12,2) DEFAULT NULL,
  `Estatus` char(1) NOT NULL ,
  `Reinvertir` char(5) DEFAULT NULL,
  `InversionRenovada` int(11) DEFAULT NULL ,
  `MonedaID` int(11) DEFAULT NULL,
  `Etiqueta` varchar(100) DEFAULT NULL,
  `SaldoProvision` decimal(12,2) DEFAULT NULL,
  `ValorGat` decimal(12,2) DEFAULT NULL,
  `Beneficiario` char(1) DEFAULT NULL,
  `ValorGatReal` decimal(12,2) DEFAULT NULL,
  `FechaVenAnt` date DEFAULT NULL ,
  `DireccionIP` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`PagareID`),
  KEY `FK_TIPOINVERION` (`TipoInversionID`),
  KEY `FK_CUENTA_INVERSION` (`CuentaID`),
  KEY `FK_CLIENTE_INVERSION` (`ClienteID`),
  KEY `FK_MONEDA_INVERSION` (`MonedaID`),
  KEY `INV_FECHAYESTAUS` (`FechaVencimiento`,`Estatus`),
  KEY `kidx_cte_cta_estatus` (`ClienteID`,`CuentaID`,`Estatus`) USING BTREE
)');

prepare stmt from @sql_text;
execute stmt;

 INSERT INTO  kubofinanciero.PAGARES_RESPALDO(`Beneficiario`, `ClienteID`, `CuentaID`, `DireccionIP`, `Estatus`, `Etiqueta`, `FechaInicio`, `FechaVenAnt`, `FechaVencimiento`, `InteresGenerado`, `InteresRecibir`, `InteresRetener`, `InversionRenovada`, `MonedaID`, `MontoInversion`, `PagareID`, `Plazo`, `Reinvertir`, `SaldoProvision`, `Tasa`, `TasaISR`, `TasaNeta`, `TipoInversionID`, `ValorGat`, `ValorGatReal`)
SELECT `Beneficiario`, `ClienteID`, `CuentaID`, `DireccionIP`, `Estatus`, `Etiqueta`, `FechaInicio`, `FechaVenAnt`, `FechaVencimiento`, `InteresGenerado`, `InteresRecibir`, `InteresRetener`, `InversionRenovada`, `MonedaID`, `MontoInversion`, `PagareID`, `Plazo`, `Reinvertir`, `SaldoProvision`, `Tasa`, `TasaISR`, `TasaNeta`, `TipoInversionID`, `ValorGat`, `ValorGatReal`
from kubofinanciero.PAGARES
WHERE ESTATUS = 'N';

    UPDATE kubofinanciero.PAGARES SET
    TASAISR = 1.45,
	INTERESRETENER = MONTOINVERSION * (1.45)*plazo/(360*100),
    INTERESRECIBIR = INTERESGENERADO - MONTOINVERSION * (1.45)*plazo/(360*100)
	WHERE ESTATUS = 'N'; 
    

SELECT 'Proceso correcto' as mensaje;

commit;

end//

delimiter ;
